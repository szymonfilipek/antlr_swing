package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;


public class MyTreeParser extends TreeParser {

	protected GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer set(String name, Integer value)
	{
		globalSymbols.setSymbol(name, value);
		return value;
	}
	protected Integer get(String name)
	{
		return globalSymbols.getSymbol(name);
	}
	protected Integer add(Integer a, Integer b)
	{
		return a + b;
	}
	protected Integer substract(Integer a, Integer b)
	{
		return a - b;
	}
	protected Integer multiply(Integer a, Integer b)
	{
		try
		{
			return a * b;
		}
		catch(Exception ex)
		{
			System.out.print(ex.getMessage());
			return a;
		}
	}
	protected Integer divide(Integer a, Integer b)
	{
		return a / b;
	}
	
}
